Evaluation script for DEFT DRATS Belief and Sentiment (BeSt) annotations
========================================================================

This repository contains data structures and an evaluation tool for DEFT
belief and sentiment annotations.

Downloading the Software
------------------------

-   Using Git (preferred):
        $ git clone git@bitbucket.org:dbauer/best_evaluation.git
-   Downloading the repository as a zip file: Go to
    https://bitbucket.org/dbauer/best\_evaluation, in the "Navigation"
    panel on the left, click on "Downloads", then "Download repository".

Compatibility and Dependencies
------------------------------

The code is compatible with Python 2.7 and 3.x and has no external
dependencies (except for the Python standard library). The scoring
script best_evaluator.py uses classes defined in deft_ere.py and
deft_best.py. These files need to be in the working directory or in a
directory specified in the PYTHONPATH environment variable.

Running the Scoring Script
--------------------------

    usage: best_evaluator.py [-h] [-p] [-s | -b] [-f] [-v] ere_file gold_file predict_file

    Scorer for DEFT belief and sentiment annotations.

    positional arguments:
      ere_file              rich ERE XML file or directory
      gold_file             gold belief and sentiment XML file or directory
      predict_file          predicted belief and sentiment XML file or directory

    optional arguments:
      -h, --help            show this help message and exit
      -p, --partial-provenance
                            give partial credit for provenance lists
      -s, --sentiment-only  score only sentiment annotations
      -b, --belief-only     score only belief annotations
      -f, --per-file        print per-file scores (batch mode only)
      -v, --verbose         show debugging output

The script can be run in single-file or in batch processing mode.

-   In single-file mode, the positional arguments must specify an
    ERE XML file, a gold BeSt XML file, and a predicted BeSt XML file.
    $ python best_evaluator.py ere_path/4f7eedf44076ea050d7db3715f9333fa.rich_ere.xml gold_path/4f7eedf44076ea050d7db3715f9333fa.best.xml predict_path/4f7eedf44076ea050d7db3715f9333fa.best.xml`
-   In batch mode, the positional arguments specify three
    directories containing a set of ERE files, gold BeSt files, and
    predicted BeSt files.
    $ python best_evaluator.py ere_path gold_path predict_path
    For each file in the gold directory that ends in *.best.xml* the 
    script finds a corresponding ERE file in the ERE directory (same
    prefix, but ending in *.rich\_ere.xml*) and predicted BeSt file
    in the directory for predicted files (identical filename as the 
    gold annotation). In batch mode, the scoring script reports both
    micro and macro averaged results. The -f parameter can be 
    specified to print per-file scores.

### Evalution Conditions (Provenance List)

There are two evaluation conditions:

-   By default, the provenance list of each private state tuple is
    scored as one-is-enough (mentioning a single object in the
    provenance list counts as full score).
-   When the -p flag is set, the scorer weights the full score by the
    F-score of the provenance list.

### Scoring Belief and Sentiment Separately

By default the script scores both belief and sentiment annotations and
reports a single result. The parameter -b limits scoring to belief
annotations (sentiment annotations in gold and prediction are ignored).
The parameter -s limits scoring to sentiment annotations.
