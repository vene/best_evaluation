import sys
import os
import argparse
import xml.etree.ElementTree as ET
from os import listdir, makedirs
from os.path import isfile, isdir, join, splitext, split, exists

def usage():
    print ("python diagnostics.py [-s] [-b] <gold dir> <pred dir>")
    print ("-s, --sentiment-only  diagnose only sentiment annotations")
    print ('-b, --belief-only     diagnose only belief annotations')


def score(list1, list2):
    '''
    calculating P, R and F score of common items of two lists
    '''
    true_pos = set(list1).intersection(set(list2))
    precision = len(true_pos) / float(len(list2)) if len(list2) > 0 else 1.0
    recall = len(true_pos) / float(len(list1)) if len(list1) > 0 else 1.0
    fscore = 2 * (precision * recall) / (precision + recall) if (precision + recall) > 0 else 0.0
    return (precision, recall, fscore, true_pos)

def get_details(annotations, elem_name, is_sentiment):
    '''
    get src-tag pair ids for each 'entity', 'relation', 'event' part
    '''
    elem_types = {}
    srctag_ids = []
    if annotations is not None:
        for elem_sb in annotations.iter(elem_name):
            tag_id = elem_sb.get('ere_id')
            if is_sentiment:
                sentiment_elem = elem_sb.find('sentiment')
                source = sentiment_elem.find('source')
                if source is not None:
                    src_id = source.get('ere_id')
                else:
                    src_id = ''
                polarity = sentiment_elem.get('polarity')
                elem_types[src_id+tag_id] = polarity
                srctag_ids.append(src_id+tag_id)
            else:
                beliefs_elem = elem_sb.find('beliefs')
                for belief in beliefs_elem.iter('belief'):
                    source = belief.find('source')
                    if source is not None:
                        src_id = source.get('ere_id')
                    else:
                        src_id = ''
                    belief_type = belief.get('type')
                    elem_types[src_id+tag_id] = belief_type
                    srctag_ids.append(src_id+tag_id)

    return (srctag_ids, elem_types)


def score_file_elems(gold_annotations, pred_annotations, elem_names, is_sentiment):
    '''
    evaluating right src and targ pair rate of a single file for each 'entity', 'relation', 'event' part
    '''
    gold_sbs = []
    pred_sbs = []
    gold_types = {}
    pred_types = {}
    gold_sb_count_by_elem = {'entity': 0, 'relation': 0, 'event': 0}
    pred_sb_count_by_elem = {'entity': 0, 'relation': 0, 'event': 0}
    right_sb_count_by_elem = {'entity': 0, 'relation': 0, 'event': 0}

    for elem_name in elem_names:
        print 'Scores for {0} part -------\n'.format(elem_name)
        #get sentiment or belief details of gold files
        gold_elem_sb = []
        gold_srctag_ids, gold_elem_types = get_details(gold_annotations, elem_name, is_sentiment)
        gold_types.update(gold_elem_types)
        gold_elem_sb.extend(gold_srctag_ids)
        gold_sbs.extend(gold_srctag_ids)

        #get sentiment or belief details of pred files
        pred_elem_sb = []
        pred_srctag_ids, pred_elem_types = get_details(pred_annotations, elem_name, is_sentiment)
        pred_types.update(pred_elem_types)
        pred_elem_sb.extend(pred_srctag_ids)
        pred_sbs.extend(pred_srctag_ids)

        precision, recall, fscore, true_pos = score(gold_elem_sb, pred_elem_sb)
        gold_sb_count_by_elem[elem_name] += len(gold_elem_sb)
        pred_sb_count_by_elem[elem_name] += len(pred_elem_sb)
        right_sb_count_by_elem[elem_name] += len(true_pos)
        print('scores of {0} part are P: {1}  R: {2}  F: {3} -------\n'.format(elem_name, precision, recall, fscore))

    return gold_sbs, pred_sbs, gold_types, pred_types, gold_sb_count_by_elem, pred_sb_count_by_elem, right_sb_count_by_elem


def diagnostics(gold_dir, pred_dir, is_sentiment):
    """
    evaluating the percentage of right (source, target) pairs of all files in predict dir
    based on the gold files. And providing precision, recall, and fscore for detecting right
    src_tag pairs, and the performance of sentiment type (pos, neg) detection.
    """
    filenames = [f for f in os.listdir(pred_dir) if f.endswith('.best.xml')]

    total_precision = []
    total_recall = []
    total_fscore = []
    total_type_rate = []
    right_type_count = 0
    total_gold_sb_count = 0
    total_pred_sb_count = 0
    total_right_sb_count = 0
    total_gold_sb_count_by_elem = {'entity': 0, 'relation': 0, 'event': 0}
    total_pred_sb_count_by_elem = {'entity': 0, 'relation': 0, 'event': 0}
    total_right_sb_count_by_elem = {'entity': 0, 'relation': 0, 'event': 0}
    if is_sentiment:
        elem_names = ['entity', 'relation', 'event']
    else:
        elem_names = ['relation', 'event']

    for f in filenames:
        print 'Evaluating file: {0} --------\n'.format(f)
        gold_path = os.path.join(gold_dir, f)
        pred_path = os.path.join(pred_dir, f)
        #if ere and best files with the same name as source file exist
        if isfile(gold_path) and isfile(pred_path):
            gold_tree = ET.parse(gold_path)
            gold_root = gold_tree.getroot()

            pred_tree = ET.parse(pred_path)
            pred_root = pred_tree.getroot()

            if is_sentiment:
                gold_annotations = gold_root.find('sentiment_annotations')
                pred_annotations = pred_root.find('sentiment_annotations')
            else:
                gold_annotations = gold_root.find('belief_annotations')
                pred_annotations = pred_root.find('belief_annotations')

            # if sentiment_flag:
            gold_sbs, pred_sbs, gold_types, pred_types, gold_sb_count_by_elem, pred_sb_count_by_elem, right_sb_count_by_elem = \
            score_file_elems(gold_annotations, pred_annotations, elem_names, is_sentiment)

            total_gold_sb_count_by_elem = {elem_name: (count + total_gold_sb_count_by_elem[elem_name]) for elem_name, count in gold_sb_count_by_elem.items()}
            total_pred_sb_count_by_elem = {elem_name: (count + total_pred_sb_count_by_elem[elem_name]) for elem_name, count in pred_sb_count_by_elem.items()}
            total_right_sb_count_by_elem = {elem_name: (count + total_right_sb_count_by_elem[elem_name]) for elem_name, count in right_sb_count_by_elem.items()}

            #final diagnostics for each single file
            precision, recall, fscore, true_pos = score(gold_sbs, pred_sbs)
            total_gold_sb_count += len(gold_sbs)
            total_pred_sb_count += len(pred_sbs)
            total_right_sb_count += len(true_pos)

            right_type_count_file = 0
            for tp in true_pos:
                if pred_types[tp] == gold_types[tp]:
                    right_type_count_file += 1
                    right_type_count += 1

            right_type_rate = right_type_count_file / float(len(true_pos)) if len(true_pos) > 0 else None
            total_precision.append(precision)
            total_recall.append(recall)
            total_fscore.append(fscore)
            total_type_rate.append(right_type_rate)
            print('Scores of the file are P: {0}  R: {1}  F: {2} RTS (right type score of sent/belf): {3} -------\n'.format(precision, recall, fscore, right_type_rate))

    print '\n =========*****========= Final Diagnostics for Source Target Pairs Detection ==========*****=========\n'
    #final diagnostics for each 'entity', 'relation', and 'event' part
    for elem_name in elem_names:
        print 'For {0} part, there are {1} and {2} sentimtents/beliefs in all gold and pred files respectively ---***---\n'.format(elem_name,\
        total_gold_sb_count_by_elem[elem_name], total_pred_sb_count_by_elem[elem_name])

        avg_p = total_right_sb_count_by_elem[elem_name] / float(total_pred_sb_count_by_elem[elem_name]) if total_pred_sb_count_by_elem[elem_name] > 0 else 0.0
        avg_r = total_right_sb_count_by_elem[elem_name] / float(total_gold_sb_count_by_elem[elem_name]) if total_gold_sb_count_by_elem[elem_name] > 0 else 0.0
        avg_f = 2 * (avg_p * avg_r) / (avg_p + avg_r) if (avg_p + avg_r) > 0 else None
        print('average micro scores of this part are P: {0}  R: {1}  F: {2} ---***---\n'.format(avg_p, avg_r, avg_f))

    #final diagnostics for all files
    avg_p_mac = sum(total_precision) / len(total_precision)
    avg_r_mac = sum(total_recall) / len(total_recall)
    avg_f_mac = 2 * (avg_p_mac * avg_r_mac) / (avg_p_mac + avg_r_mac)
    avg_rts = right_type_count / float(total_right_sb_count) if total_right_sb_count > 0 else 0.0
    print('Average macro scores of the pred dir are P: {0}  R: {1}  F: {2} RTS(right type score of sent/belf with {3} examples): {4} ---***---\n'.format(avg_p_mac, avg_r_mac, avg_f_mac, right_type_count, avg_rts))

    avg_p_mic = total_right_sb_count / float(total_pred_sb_count) if total_pred_sb_count > 0 else 0.0
    avg_r_mic = total_right_sb_count / float(total_gold_sb_count) if total_gold_sb_count > 0 else 0.0
    avg_f_mic = 2 * (avg_p_mic * avg_r_mic) / (avg_p_mic + avg_r_mic) if (avg_p_mic + avg_r_mic) > 0 else 0.0
    print('Average micro scores of the pred dir (with {0} gold and {1} pred sent/belf examples) are P: {2}  R: {3}  F: {4} RTS ({5}): {6} ---***---\n'.format(total_gold_sb_count, total_pred_sb_count, avg_p_mic, avg_r_mic, avg_f_mic, right_type_count, avg_rts))


def main():
    parser = argparse.ArgumentParser(description='Diagnostics for DEFT belief and sentiment annotations.')
    parser.add_argument('gold_file', type=str, help = 'gold belief and sentiment XML file or directory')
    parser.add_argument('predict_file', type=str, help = 'predicted belief and sentiment XML file or directory')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-s','--sentiment-only', default=False, dest='sentiment', action='store_true', help='score only sentiment annotations')
    group.add_argument('-b','--belief-only', default=False, dest='belief', action='store_true', help='score only belief annotations')
    args = parser.parse_args()

    if os.path.isdir(args.gold_file) and os.path.isdir(args.predict_file):
        print('Evaluating all files in the specified directories.\n')

        if args.belief:
            print('\n---***---diagnostics (belief only)---***---')
            diagnostics(args.gold_file, args.predict_file, False)
        elif args.sentiment:
            print('\n---***---diagnostics (sentiment only)---***---')
            diagnostics(args.gold_file, args.predict_file, True)
        else:
            print('\n---***---diagnostics---***---')
            print('\n---***---diagnostics (belief only)---***---')
            diagnostics(args.gold_file, args.predict_file, False)
            print('\n---***---diagnostics (sentiment only)---***---')
            diagnostics(args.gold_file, args.predict_file, True)
    else:
        usage()

if __name__ == "__main__":
    main()