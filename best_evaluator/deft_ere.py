#!/usr/bin/env python
"""
This module contains classes to represent DEFT ERE annotations and methods
to read ERE XML annotations into an OOP representation.
"""
from __future__ import print_function

import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
from collections import defaultdict
import sys
import re

__author__ = 'Daniel Bauer'
__email__ = 'bauer@cs.columbia.edu'
__date__ = 'May 31 2016'


offset_range_re = re.compile("(.*)?_\d+\-\d+$")


class AnnotationFormatError(RuntimeError):
    pass


class DocumentSource(object):
    """
    a document source
    """
    def __init__(self, kit_id, doc_id, source_type='multi_post'):
        self.kit_id = kit_id
        self.doc_id = doc_id
        self.source_type = source_type


#Classes representing object types

class Entity(object):
    """
    an individul entity.
    """
    def __init__(self, eid, etype, specificity='specific', source_doc=None):
        self.entity_id = eid
        self.entity_type = etype
        self.specificity = specificity
        self.mentions = []

    def __repr__(self):
        return '<Entity {0}>'.format(self.entity_id)

    def to_element_tree(self):
        element = ET.Element('entity', {'id':self.entity_id, 'type':self.entity_type, 'specificity':self.specificity})
        for mention in self.mentions:
            element.append(mention.to_element_tree())
        return element


class Relation(object):
    """
    an individual relation.
    """
    def __init__(self, rid, rtype, rsubtype):
        self.relation_id = rid
        self.relation_type = rtype
        self.relation_subtype = rsubtype
        self.mentions = []

    def __repr__(self):
        return '<Relation {0}>'.format(self.relation_id)

    def to_element_tree(self):
        element = ET.Element('relation', {'id':self.relation_id, 'type':self.relation_type, 'subtype':self.relation_subtype})
        for mention in self.mentions:
            element.append(mention.to_element_tree())
        return element


class Hopper(object):
    """
    an individual event.
    """
    def __init__(self, eid):
        self.event_id = eid
        self.mentions = []

    def __repr__(self):
        return '<Hopper {0}>'.format(self.event_id)

    def to_element_tree(self):
        element = ET.Element('hopper', {'id':self.event_id})
        for mention in self.mentions:
            element.append(mention.to_element_tree())
        return element

class Filler(object):
    """
    an individual filler.
    """
    def __init__(self, fid, offset, length, ftype):
        self.filler_id = fid
        self.offset = offset
        self.length = length
        self.filler_type = ftype
        self.text = ''

    def __repr__(self):
        return '<Filler {0}>'.format(self.filler_id)





#Objects representing individual mentions
class EntityMention(object):
    """
    a single mention of an entity
    """
    def __init__(self, mention_id, noun_type, source, offset, length, entity = None):
        self.mention_id = mention_id
        self.noun_type = noun_type
        self.source = source
        self.offset = offset
        self.length = length
        self.entity = entity # reference to the entity type of this mention
        self.mention_text = ''
        self.nom_head = None
        self.beliefs = [] # beliefs for which this entity mention is the source
        self.argument_beliefs = [] # argument beliefs for which this entity mention is the source
        self.has_sentiments = [] # sentiments for which this entity mention is the source
        self.sentiments = [] # sentiments towards this entity

    def __repr__(self):
        return '<EntityMention {0}>'.format(self.mention_id)

    def to_element_tree(self):
        element = ET.Element('entity_mention', {'id':self.mention_id, 'noun_type':self.noun_type, 'source':self.source.doc_id, 'offset':str(self.offset), 'length':str(self.length)})
        textelement = ET.Element('mention_text')
        textelement.text = self.mention_text
        element.append(textelement)
        if self.nom_head is not None:
            element.append(self.nom_head.to_element_tree())

        return element


class NomHead(object):
    """
    a nominal head description
    """
    def __init__(self, offset, length, text, source):
        self.offset = offset
        self.length = length
        self.text = text
        self.source = source

    def to_element_tree(self):
        element = ET.Element('nom_head', {'source':self.source.doc_id, 'offset':str(self.offset), 'length':str(self.length)})
        element.text = self.text
        return element

class RelationMention(object):
    """
    a specific mention of a relation.
    """
    def __init__(self, mention_id, realis):
        self.mention_id = mention_id
        self.realis = realis if realis else "unknown"
        self.rel_arg1 = None
        self.rel_arg2 = None
        self.trigger = None
        self.relation = None
        self.beliefs = []
        self.sentiments = []

    def __repr__(self):
        return '<RelationMention {0}({1}) {2} {3}>'.format(self.mention_id, self.relation.relation_id, self.rel_arg1, self.rel_arg2)

    def to_element_tree(self):
        element = ET.Element('relation_mention', {'id':self.mention_id, 'realis':self.realis})
        if self.rel_arg1:
            mention = self.rel_arg1.entity_mention
            entity = mention.entity
            arg1_element = ET.Element('rel_arg1',{'entity_id':entity.entity_id, 'entity_mention_id':mention.mention_id, 'role':self.rel_arg1.role})
            element.append(arg1_element)
        if self.rel_arg2:
            mention = self.rel_arg2.entity_mention
            if mention:
                entity = mention.entity
                arg2_element = ET.Element('rel_arg2',{'entity_id':entity.entity_id, 'entity_mention_id':mention.mention_id, 'role':self.rel_arg2.role})
            else:
                arg2_element = ET.Element('rel_arg2',{'entity_id':'null', 'entity_mention_id':'null', 'role':self.rel_arg2.role})
            element.append(arg2_element)
        if self.trigger:
            element.append(self.trigger.to_element_tree())

        return element

class Argument(object):
    """
    argument of a relation or event
    """
    def __init__(self, entity, entity_mention, role, realis = None, is_filler= False):
        self.entity = entity
        self.entity_mention = entity_mention
        if entity_mention:
            self.mention_id = entity_mention.mention_id
        else:
            assert isinstance(entity, Filler)
            self.mention_id = entity.filler_id
        self.role = role
        self.realis = realis if realis else 'unknown'
        self.text = ''
        self.is_filler = is_filler
        self.beliefs = []

    def __repr__(self):

        if self.is_filler:
            return '<Argument mention: {0}, role: {1}>'.format(self.entity, self.role)
        else:
            if self.entity_mention:
                return '<Argument mention: {0}, role: {1}>'.format(self.entity_mention.mention_id, self.role)
            else:
                return '<Argument mention: {0}, role: {1}>'.format("NULL", self.role)

class EventMention(object):
    """
    a specific mention of an event.
    """
    def __init__(self, mention_id, etype, esubtype, realis):
        self.mention_id = mention_id
        self.event_type = etype
        self.event_subtype = esubtype
        self.realis = realis if realis else 'unknown'
        self.arguments = {} # role => Argument objects
        self.argument_index = defaultdict(list) # entity ID => list of Argument objects
        self.trigger = None
        self.hopper = None
        self.beliefs = []
        self.sentiments = []

    def __repr__(self):
        argstr = ",".join([str(x) for x in self.arguments.values()])
        return '<EventMention {0}({1}) {2}>'.format(self.mention_id, self.hopper.event_id, argstr)

    def to_element_tree(self):
        element = ET.Element('event_mention', {'id':self.mention_id, 'type':self.event_type, 'subtype':self.event_subtype, 'realis':self.realis})

        if self.trigger:
            trigger_element = self.trigger.to_element_tree()
            element.append(trigger_element)

        for entity_id in self.argument_index:
            args = self.argument_index[entity_id]
            for arg in args:

                if arg.is_filler:

                    argelement = ET.Element('em_arg', {'filler_id':arg.entity.filler_id, 'role':arg.role, 'realis':arg.realis})
                else:
                    argelement = ET.Element('em_arg', {'entity_id':entity_id, 'entity_mention_id':arg.entity_mention.mention_id, 'role':arg.role, 'realis':arg.realis})

                if arg.text:
                   argelement.text = arg.text
                element.append(argelement)
        return element


class Trigger(object):
    """
    Trigger of a relation or event mention.
    """
    def __init__(self, source, offset, length):
        self.source = source
        self.offset = offset
        self.length = length
        self.text = ''


    def to_element_tree(self):
        trigger_et = ET.Element('trigger', {'source':self.source.doc_id, 'offset':str(self.offset), 'length':str(self.length)})
        if self.text:
            trigger_et.text = self.text
        return trigger_et

##############################################################################
# The following section contains a top-level class EREAnnotations that stores
# ERE annotationsand functions to populate this object from an XML source.

class EREAnnotations(object):
    """
    Represent a set of EREAnnotations, possibly over multiple documents
    """


    def __init__(self):
        self.sources = {}
        self.entities = {}
        self.entity_mentions = {}
        self.fillers = {}
        self.relations = {}
        self.relation_mentions = {}
        self.hoppers = {}
        self.event_mentions = {}

    def to_element_tree(self):
        if len(self.sources)>1:
            raise NotImplementedError("Don't know how to XML-ify multiple sources.")

        docsrc = self.sources.values()[0]

        element = ET.Element('deft_ere', {'kit_id':docsrc.kit_id, 'doc_id':docsrc.doc_id, 'source_type':docsrc.source_type})

        entity_element = ET.Element('entities')
        for entity in self.entities.values():
            entity_element.append(entity.to_element_tree())
        element.append(entity_element)

        relation_element = ET.Element('relations')
        for relation in self.relations.values():
            relation_element.append(relation.to_element_tree())
        element.append(relation_element)

        event_element = ET.Element('hoppers')
        for event in self.hoppers.values():
            event_element.append(event.to_element_tree())
        element.append(event_element)

        return element


    def integrate_entities(self, etree, source):
        """
        Add the entities described in this etree object into the annotation data structure.
        Note that if an entity element uses an existing id, its entity mentions will just be
        added to the original entity object.
        """
        for entity_et in etree:
            assert entity_et.tag == 'entity'

            entity = Entity(entity_et.get('id'), entity_et.get('type'), entity_et.get('specificity'), source)
            if not self.entities.get(entity.entity_id):
                self.entities[entity.entity_id] = entity
            else:
                entity = self.entities.get(entity.entity_id)

            for mention_et in entity_et:  # add each mention id
                assert mention_et.tag == 'entity_mention'

                try:
                    source = self.sources[mention_et.get('source')]
                except KeyError:
                    source = None
                mention = EntityMention(mention_et.get('id'), mention_et.get('noun_type'), source,
                                        int(mention_et.get('offset')), int(mention_et.get('length')), entity)
                if not mention.mention_id in self.entity_mentions:
                    self.entity_mentions[mention.mention_id] = mention
                    entity.mentions.append(mention)
                else:
                    mention = self.entity_mentions[mention.mention_id]

                for child_et in mention_et:
                    if child_et.tag == 'mention_text':
                        mention.mention_text = child_et.text
                    elif child_et.tag == 'nom_head':
                        mention.nom_head = NomHead(int(child_et.get('offset')), int(child_et.get('length')), child_et.text, self.sources[child_et.get('source')])
                    else:
                        raise AnnotationFormatError('unexpected XML element {0}, child of {1}.'.format(tostring(child_et), tostring(mention_et)))

    def integrate_relations(self, etree, source):
        """
        Add the relations described in this etree object into the annotation data structure.
        """
        for relation_et in etree:
            assert relation_et.tag == 'relation'

            relation = Relation(relation_et.get('id'), relation_et.get('type'), relation_et.get('subtype'))
            if not relation.relation_id in self.relations:
                self.relations[relation.relation_id] = relation
            else:
                relation = self.relations[relation.relation_id]

            for mention_et in relation_et:
                assert mention_et.tag == 'relation_mention'
                mention = RelationMention(mention_et.get('id'), mention_et.get('realis'))
                if not mention.mention_id in self.relation_mentions:
                    self.relation_mentions[mention.mention_id] = mention
                    relation.mentions.append(mention)
                    mention.relation = relation
                else:
                    mention = self.relation_mentions[mention.mention_id]


                for child_et in mention_et:
                    if child_et.tag == 'rel_arg1':
                        if child_et.get('entity_id') is not None:
                            entity = self.entities[child_et.get('entity_id')]
                            if not child_et.get('entity_mention_id') in self.entity_mentions:
                                print(child_et)
                            entity_mention = self.entity_mentions[child_et.get('entity_mention_id')]
                            assert entity is entity_mention.entity
                            arg = Argument(entity, entity_mention, child_et.get('role'))
                            arg.text = child_et.text
                            mention.rel_arg1 = arg
                        elif child_et.get('filler_id') is not None:
                            filler = self.fillers[child_et.get('filler_id')]
                            arg = Argument(filler, None, child_et.get('role'))
                            arg.text = child_et.text
                            mention.rel_arg1 = arg
                        else:
                            raise AnnotationFormatError('Invalid attributes for XML element {0}. Expected entity_id or filler_id.'.format(tostring(child_et)))

                    elif child_et.tag == 'rel_arg2':
                        if child_et.get('entity_id') is not None:
                            entity = self.entities[child_et.get('entity_id')]
                            entity_mention = self.entity_mentions[child_et.get('entity_mention_id')]
                            assert entity is entity_mention.entity
                            arg = Argument(entity, entity_mention, child_et.get('role'))
                            arg.text = child_et.text
                            mention.rel_arg2 = arg
                        elif child_et.get('filler_id') is not None:
                            filler = self.fillers[child_et.get('filler_id')]
                            arg = Argument(filler, None, child_et.get('role'))
                            arg.text = child_et.text
                            mention.rel_arg2 = arg
                        else:
                            raise AnnotationFormatError('Invalid attributes for XML element {0}. Expected entity_id or filler_id.'.format(tostring(child_et)))


                    elif child_et.tag == 'trigger':
                        source = self.sources[child_et.get('source')]
                        trigger = Trigger(source, int(child_et.get('offset')), int(child_et.get('length')))
                        trigger.text = child_et.text
                        mention.trigger = trigger
                    else:
                        raise AnnotationFormatError('unexpected XML element {0}, child of {1}.'.format(tostring(child_et), tostring(mention_et)))

    def integrate_hoppers(self, etree, source):
        """
        Add the events ("hoppers") described in this etree object into the annotation data structure.
        """
        for hopper_et in etree:
            assert hopper_et.tag == 'hopper'

            hopper = Hopper(hopper_et.get('id'))
            if not hopper.event_id in self.hoppers:
                self.hoppers[hopper.event_id] = hopper
            else:
                hopper = self.hoppers[hopper.event_id]

            for mention_et in hopper_et:
                assert mention_et.tag == 'event_mention'
                mention = EventMention(mention_et.get('id'), mention_et.get('type'), mention_et.get('subtype'), mention_et.get('realis'))
                if not mention.mention_id in self.event_mentions:
                    self.event_mentions[mention.mention_id] = mention
                    mention.hopper= hopper
                    hopper.mentions.append(mention)
                else:
                    mention = self.event_mentions[mention.mention_id]

                for child_et in mention_et:
                    if child_et.tag == 'em_arg':
                        if child_et.get('filler_id'): # Some mentions have filler arguments instead of entities
                            filler = self.fillers[child_et.get('filler_id')]
                            arg = Argument(filler, None, child_et.get('role'), child_et.get('realis'), is_filler = True)
                            mention.arguments[arg.role] = arg
                            mention.argument_index[arg.entity.filler_id].append(arg)
                        else:
                            entity = self.entities[child_et.get('entity_id')]
                            entity_mention = self.entity_mentions[child_et.get('entity_mention_id')]
                            assert entity is entity_mention.entity
                            arg = Argument(entity, entity_mention, child_et.get('role'), child_et.get('realis'))
                            mention.arguments[arg.entity_mention.mention_id] = arg
                            mention.argument_index[arg.entity_mention.mention_id].append(arg)
                        arg.text = child_et.text
                    elif child_et.tag == 'trigger':
                        source = self.sources[child_et.get('source')]
                        trigger = Trigger(source, int(child_et.get('offset')), int(child_et.get('length')))
                        trigger.text = child_et.text
                        mention.trigger = trigger
                    else:
                        raise AnnotationFormatError('unexpected XML element {0}, child of {1}.'.format(tostring(child_et), tostring(mention_et)))

    def integrate_fillers(self, etree, source):
        """
        Add the fillers described in this etree object into the annotation data structure.
        """
        for filler_et in etree:
            assert filler_et.tag == 'filler'
            filler = Filler(filler_et.get('id'), int(filler_et.get('offset')), int(filler_et.get('length')), filler_et.get('type'))
            if not filler.filler_id in self.fillers:
                self.fillers[filler.filler_id] = filler
            else:
                filler = self.fillers[filler.filler_id]
            filler.text = filler_et.text


    def integrate_etree(self, etree):
        """
        Add the content of an e-tree object into the annotation data structure.
        """
        root = etree.getroot()
        # Create a new source object


        docid = root.get('doc_id')
        match =  offset_range_re.match(docid)
        if match:
            docid = match.group(1)

        source = DocumentSource(root.get('kit_id'), docid, root.get('source_type'))
        if not source.doc_id in self.sources:
            self.sources[source.doc_id] = source
        else:
            source = self.sources[source.doc_id]

        # Now process the children
        for child in root:
            if child.tag == 'entities':
                self.integrate_entities(child, source)
            elif child.tag == 'fillers':
                self.integrate_fillers(child, source)
            elif child.tag == 'relations':
                self.integrate_relations(child, source)
            elif child.tag == 'hoppers':
                self.integrate_hoppers(child, source)
            else:
                raise AnnotationFormatError('unexpected XML element {0} at top level.'.format(tostring(child)))


    def apply_ent_map(self, entity_map, em_map):
        """
        Replace all entity IDs.
        """

        new_entity_mentions = {}
        for em_id in self.entity_mentions:
            if em_id in em_map:
                em = self.entity_mentions[em_id]
                em.mention_id = em_map[em_id]
                new_entity_mentions[em.mention_id] = em
        self.entity_mentions = new_entity_mentions

        new_entities = {}
        for old_id in entity_map:
            new_id = entity_map[old_id]
            entity = self.entities[old_id]
            entity.entity_id = new_id
            new_entities[new_id] = entity
        self.entities = new_entities

        for event_men in self.event_mentions:
            event_mention = self.event_mentions[event_men]
            #event_mention.argument_index = defaultdict(list)
            new_argument_index = defaultdict(list)
            new_arguments = {}
            for em_id in event_mention.arguments:
                arg = event_mention.arguments[em_id]
                if arg.entity_mention:  #ignore fillers
                    new_arguments[arg.entity_mention.mention_id] = arg
                    new_argument_index[arg.entity_mention.entity.entity_id].append(arg)
                    #event_mention.argument_index[arg.entity].append(arg)

            event_mention.arguments = new_arguments
            event_mention.argument_index = new_argument_index

    def apply_rel_map(self, rel_map, relm_map):
        new_relation_mentions = {}
        for relm_id in self.relation_mentions:
            relm = self.relation_mentions[relm_id]
            if relm_id in relm_map:
                relm.mention_id = relm_map[relm_id]
                new_relation_mentions[relm.mention_id] = relm


        self.relation_mentions = new_relation_mentions

        new_relations = {}
        for old_id in rel_map:
            new_id = rel_map[old_id]
            relation = self.relations[old_id]
            relation.relation_id = new_id
            new_relations[new_id] = relation
        self.relations = new_relations

    def apply_event_map(self, event_map, eventm_map):
        new_event_mentions = {}
        for eventm_id in self.event_mentions:
            eventm = self.event_mentions[eventm_id]
            eventm.mention_id = eventm_map[eventm_id]
            new_event_mentions[eventm.mention_id] = eventm
        self.event_mentions = new_event_mentions

        new_events = {}
        for old_id in event_map:
            new_id = event_map[old_id]
            hopper = self.hoppers[old_id]
            hopper.event_id = new_id
            new_events[new_id] = hopper
        self.hoppers = new_events


def read_ere_xml(*sources):
    """
    Read in one or more XML sources and return an EREAnnotation object.
    """

    annotations = EREAnnotations(); # The new annotations object

    for source_f in sources:
        et = ET.parse(source_f)
        annotations.integrate_etree(et)

    return annotations

if __name__=="__main__":
    annos = read_ere_xml(sys.argv[1])
